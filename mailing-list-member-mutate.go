package email

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) MailingListMemberNew(list uuid.UUID, name, address string, metadata map[string]interface{}) error {
	d := marshal(map[string]interface{}{
		"list":     list,
		"name":     name,
		"address":  address,
		"metadata": metadata,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/api/list/member/new", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) MailingListMemberUpdate(list uuid.UUID, member, name, newAddress string, metadata map[string]interface{}) error {
	d := marshal(map[string]interface{}{
		"list":       list,
		"address":    member,
		"name":       name,
		"newAddress": newAddress,
		"metadata":   metadata,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/api/list/member/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) MailingListMemberDelete(id uuid.UUID, member string) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/api/list/member/delete?id=%v&member=%v", c.proto, c.base, id, member), _json, nil, nil, responseNull, nil)
	return err
}
