package email

import (
	uuid "github.com/satori/go.uuid"
)

// Connection is an instance of a connection to the bronzecod Email server
// each function corresponds to a synchronous API call to the server
type Connection interface {
	// send interface
	SendMessage(msg Message) error

	// template api
	TemplateList() ([]Template, error)
	TemplateGet(id uuid.UUID) (Template, error)
	TemplateNew(header, footer *uuid.UUID, name, subjectLine string, content []byte, contentType TemplateType, dependencies []uuid.UUID) (Template, error)
	TemplateUpdate(tpl Template) error
	TemplateDelete(id uuid.UUID) error

	// mailling list api
	MailingListGet(id uuid.UUID) (MailingList, error)
	MailingListNew(address, name, description string) (MailingList, error)
	MailingListUpdate(id uuid.UUID, address, name, description string) (MailingList, error)
	MailingListDelete(id uuid.UUID) error

	// mailling list members
	MailingListMemberNew(list uuid.UUID, name, address string, metadata map[string]interface{}) error
	MailingListMemberUpdate(list uuid.UUID, member, name, newAddress string, metadata map[string]interface{}) error
	MailingListMemberDelete(id uuid.UUID, member string) error
}
