package email

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) TemplateList() ([]Template, error) {
	var res []Template
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/api/template/list", c.proto, c.base), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) TemplateGet(id uuid.UUID) (Template, error) {
	var res Template
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/api/template/get?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &res)
	return res, err
}
