package email

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) MailingListNew(address, name, description string) (MailingList, error) {
	var res MailingList
	d := marshal(map[string]interface{}{
		"address":     address,
		"name":        name,
		"description": description,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/api/list/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) MailingListUpdate(id uuid.UUID, address, name, description string) (MailingList, error) {
	var res MailingList
	d := marshal(map[string]interface{}{
		"id":          id,
		"address":     address,
		"name":        name,
		"description": description,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/api/list/update", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) MailingListDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/api/list/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}
