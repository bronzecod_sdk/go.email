package email

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Template represents a prepared email message that can be populated with
// provided data when a send request is made
type Template struct {
	ID           uuid.UUID    `json:"id"`
	CreatedAt    time.Time    `json:"createdAt"`
	UpdatedAt    time.Time    `json:"updatedAt"`
	Name         string       `json:"name"`
	Type         TemplateType `json:"type"`
	Subject      string       `json:"subject"`
	Content      []byte       `json:"content"`
	Header       *uuid.UUID   `json:"header"`
	Footer       *uuid.UUID   `json:"footer"`
	Dependencies []uuid.UUID  `json:"dependencies"`
}

// TemplateType identifies the supported content types of templates
type TemplateType string

const (
	// TypeRawHTML indicates that the content is string encoded html ready for templating and sending
	TypeRawHTML TemplateType = "html"
	// TypeMarkdown indicates that the content is in markdown (commonmark) format and requires pre-processing before sending
	TypeMarkdown TemplateType = "md"
)
