package email

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) MailingListGet(id uuid.UUID) (MailingList, error) {
	var res MailingList
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/api/list/get?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &res)
	return res, err
}
