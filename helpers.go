package email

import (
	uuid "github.com/satori/go.uuid"
)

// String is a helper to convert a string to a pointer to string
func String(str string) *string {
	return &str
}

// UUID is a helper to convert a UUID to a pointer to UUID
func UUID(id uuid.UUID) *uuid.UUID {
	return &id
}
