module bitbucket.org/bronzecod_sdk/go.email

go 1.18

require (
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
)

require gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
