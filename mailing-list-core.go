package email

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// MailingList is a construct for grouping a recurring set of recipients and their associated metadata
type MailingList struct {
	ID           uuid.UUID           `json:"id"`
	CreatedAt    time.Time           `json:"createdAt"`
	UpdatedAt    time.Time           `json:"updatedAt"`
	Name         string              `json:"name"`
	AddressAlias string              `json:"address"`
	Description  string              `json:"description"`
	UserCount    int                 `json:"userCount"`
	Members      []MailingListMember `json:"members"`
}

// MailingListMember represents a single recipient in a MailingList
type MailingListMember struct {
	Name     string                 `json:"name"`
	Address  string                 `json:"address"`
	Metadata map[string]interface{} `json:"metadata"`
}
