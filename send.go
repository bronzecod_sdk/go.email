package email

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

// Message describes an email message to send, stipulating;
//  - the domain to send from, if different than your platforms default
//  - the sender address
//  - a list of recipients, optionally bcc recipients
//  - the message content, optionally from a template or raw content
//  - a list of any attachments
type Message struct {
	Domain      *uuid.UUID   `json:"domain"`
	From        string       `json:"from"`
	To          []string     `json:"to"`
	Bcc         []string     `json:"bcc"`
	Subject     *string      `json:"subject"`
	Body        *string      `json:"body"`
	Template    *uuid.UUID   `json:"template"`
	Attachments []Attachment `json:"attachments"`

	Data map[string]interface{} `json:"data"`
}

// Attachment represents an attachment to an email, with a given filename and contents
type Attachment struct {
	Name    string `json:"name"`
	Content []byte `json:"content"`
}

func (c *connection) SendMessage(msg Message) error {
	d := marshal(msg)
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/api/send", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}
