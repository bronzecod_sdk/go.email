package email

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) TemplateNew(header, footer *uuid.UUID, name, subjectLine string, content []byte, contentType TemplateType, dependencies []uuid.UUID) (Template, error) {
	var res Template
	d := marshal(Template{
		Header:       header,
		Footer:       footer,
		Name:         name,
		Type:         contentType,
		Subject:      subjectLine,
		Content:      content,
		Dependencies: dependencies,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/api/template/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) TemplateUpdate(tpl Template) error {
	d := marshal(tpl)
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/api/template/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) TemplateDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/api/template/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}
