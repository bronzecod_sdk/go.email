package email

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Connection to the Bronzecod Email SDK
type connection struct {
	conn  http.Client
	base  string
	user  string
	pass  string
	proto string
}

const (
	_json          = "application/json"
	responseNull   = "null"
	responseString = "string"
	responseJSON   = "json"
)

// Dial a new connection to a bronzecod Email instance
func Dial(baseurl, user, password string, tls *tls.Config) (Connection, error) {
	Conn := &connection{
		conn: http.Client{
			Timeout: time.Second * 5,
			Transport: &http.Transport{
				TLSClientConfig: tls,
			},
		},
		base: baseurl,
		user: user,
		pass: password,
	}

	if tls != nil {
		Conn.proto = "https"
	} else {
		Conn.proto = "http"
		if strings.HasPrefix(baseurl, "https") {
			Conn.proto = "https"
		}
	}
	Conn.base = strings.TrimPrefix(Conn.base, "https://")
	Conn.base = strings.TrimPrefix(Conn.base, "http://")

	err := Conn.test()
	if err != nil {
		return Conn, errors.Wrap(err, "Invalid credentials or invalid request source for API whitelist")
	}

	return Conn, nil
}

func (c *connection) test() error {
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/test", c.proto, c.base), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) request(method, url, contentType string, headers map[string]string, body []byte) (*http.Response, error) {
	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(c.user, c.pass)
	req.Header.Set("Content-Type", contentType)
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	return c.conn.Do(req)
}

func (c *connection) processRequest(method, url, contentType string, headers map[string]string, body []byte, expectedType string, response interface{}) error {
	resp, err := c.request(method, url, contentType, headers, body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode < http.StatusOK || resp.StatusCode >= http.StatusMultipleChoices {
		var buf bytes.Buffer
		buf.ReadFrom(resp.Body)
		return errors.New(buf.String())
	}
	switch expectedType {
	case responseString:
		// HACK: short term work around to assign to strings
		var buf bytes.Buffer
		buf.ReadFrom(resp.Body)
		err = json.Unmarshal([]byte("\""+buf.String()+"\""), response)
		return err
	case responseJSON:
		err = json.NewDecoder(resp.Body).Decode(response)
		return err
	case responseNull:
		return nil
	default:
		return errors.New("Unexpected response type")
	}
}

func marshal(data interface{}) []byte {
	resp, _ := json.Marshal(data)
	return resp
}
